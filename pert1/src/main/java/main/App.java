package main;

import static spark.Spark.get;

public class App {
    public static void main(String[] args){
        get("/hello", (req, res)->"Hello World");
//        get("/hello/:name", (req, res)->"Hello, "+req.params("name"));
//        get("/hello/:name/:umur/:alamat", (req, res)->{
//            String name = req.params("name");
//            String umur = req.params("umur");
//            String alamat = req.params(":alamat");
//            int umr = Integer.parseInt(umur);
//            return "Hello, "+name+" <br> Umur saya "+umr+" tahun <br>Saya berasal dari : <strong style='color: blue;'>"+alamat+"</strong>";
//        });
        get("hello/:a/:operator/:b", (req, res)->{
            String a = req.params("a");
            String b = req.params("b");
            String operator = req.params("operator");
            int par1 = Integer.parseInt(a);
            int par2 = Integer.parseInt(b);
            int hasil;
            if(operator.equalsIgnoreCase("kali")){
                hasil = par1*par2;
                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
            }else if(operator.equalsIgnoreCase("bagi")){
                hasil = par1/par2;
                return hasil;
            }else if(operator.equalsIgnoreCase("kurang")){
                hasil = par1-par2;
                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
            }else if(operator.equalsIgnoreCase("tambah")){
                hasil = par1+par2;
                return "hasil dari "+a+" "+operator+" "+b+" = "+hasil;
            }else{
                return "Maaf masukan operator berupa kata, bukan simbol";
            }
        });
    }
}
